import { useState, useCallback } from "react";

const useHttp = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const request = useCallback(
    async (url, method = "GET", body = null, headers = {}) => {
      setError(null);
      setIsLoading(true);
      try {
        if (typeof body === "object" && body !== null) {
          body = JSON.stringify(body);
          headers["Content-Type"] = "application/json";
        } else {
          setError("Body is null or not an object");
          setIsLoading(false);
          return {
            message: "Error",
            success: false,
          };
        }
        const response = await fetch(url, { method, body, headers });
        const data = await response.json();

        if (!response.ok) {
          setError(response.statusText || "Something is went wrong");
        }

        setIsLoading(false);
        return data;
      } catch (e) {
        setIsLoading(false);
        setError(e.message);
      }
    },
    []
  );

  return {
    isLoading,
    request,
    error,
  };
};

const requestHandler = async (request, body) => {
  try {
    return await request("/api/movies/", "POST", body);
  } catch (e) {
    return {
      success: false,
      message: e.message,
    };
  }
};

export { useHttp };
export { requestHandler };
