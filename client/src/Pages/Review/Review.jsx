import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import Inputmask from "inputmask";
import Toast from "../../Components/Toast/Toast";
import starWarsLogo from "../../../media/starWars.jpg";
import noReviewsLogo from "../../../media/noReviews.svg";
import s from "./Review.scss";

const Review = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [review, setReview] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [showToast, setShowToast] = useState(false);

  useEffect(() => {
    const selector = document.getElementById("email");
    const im = new Inputmask({ mask: "*{2,20}@*{2,20}.*{2,7}", greedy: false });
    im.mask(selector);
  }, []);

  const getHeader = () => {
    return (
      <div className="container-pd-0 justify-content-start">
        <img src={starWarsLogo} className="img-fluid" alt="Responsive image" />
      </div>
    );
  };

  const getTitle = () => {
    return (
      <h3 className={`${s.displayCustom} mx-3 mt-2 mb-1`}>
        {`Add review about "${localStorage.getItem("movie")}" movie`}
      </h3>
    );
  };

  const getReviewForm = () => {
    return (
      <div className="container-fluid">
        <form role="form">
          <div className="row g-3 mb-2">
            <div className="col">
              <textarea
                className="form-control"
                rows="5"
                onChange={(e) => setReview(e.target.value)}
              ></textarea>
            </div>
          </div>
          <div className="row g-3 mb-2">
            <div className="col-auto input-group-sm mb-2">
              <input
                className="input-group form-control"
                type="text"
                placeholder="Name"
                name="username"
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div className="col-auto input-group-sm">
              <input
                className="input-group form-control"
                type="text"
                name="email"
                placeholder="Example@mail.ru"
                id="email"
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
          </div>
          <div className="row g-3 mb-2">
            <div className="col">
              <button
                type="submit"
                onClick={addReview}
                className="btn btn-primary"
                disabled={disabled}
              >
                {"Add review"}
                {isLoading && (
                  <span
                    class="spinner-border spinner-border-sm ml-2"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  };

  const getReviews = () => {
    const reviews = JSON.parse(localStorage.getItem("reviews")) || {};
    const movie = localStorage.getItem("movie");
    if (reviews[movie]) {
      return (
        <div className="container-fluid">
          <div className="row border-top">
            <div className="col">
              <h1 className={s.displayCustom}>{"Reviews"}</h1>
            </div>
          </div>
          {reviews[movie].map((item, index) => (
            <div className="row border-top" key={index}>
              <div className="col my-2">
                <div className="row">
                  <div className="col">
                    <span>{item.name}</span>
                  </div>
                </div>
                <div className="row text-muted font-weight-light mb-1">
                  <div className={`col ${s.reviewItemEmail}`}>
                    <span>{item.email}</span>
                  </div>
                </div>
                <div className="row">
                  <div className="col font-weight-light">
                    <span>{item.review}</span>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      );
    } else {
      return (
        <div className="border-top pt-2 d-flex justify-content-center align-items-center">
          <img src={noReviewsLogo} alt="No reviews image" />
          <span className="mx-3 text-muted">{"Be the first to review!"}</span>
        </div>
      );
    }
  };

  const addReview = (e) => {
    e.preventDefault();
    setIsLoading(true);
    setDisabled(true);

    setTimeout(async () => {
      const movie = localStorage.getItem("movie");
      const reviews = localStorage.reviews && JSON.parse(localStorage.reviews) || { [movie]: [] };

      const pushReview = () => {
        if (reviews[movie]) {
          reviews[movie].push({ name, email, review });
        } else {
          reviews[movie] = [{ name, email, review }];
        }
      };

      pushReview();

      localStorage.setItem("reviews", JSON.stringify(reviews));

      setIsLoading(false);
      setShowToast(true);
    }, 2000);
  };

  return (
    <>
      <div className="container shadow-sm px-3 mb-5 bg-white rounded">
        <div className={`row ${s.reviewBack}`}>
          <div className="col">
            <div className="row mb-2">{getHeader()}</div>
            <div className="row mb-2">
              <div className="col">{getTitle()}</div>
            </div>
            <div className="row mb-2">
              <div className="col">{getReviewForm()}</div>
            </div>
            <div className="row mb-2">
              <div className="col">{getReviews()}</div>
            </div>
          </div>
        </div>
      </div>
      <Toast
        className="m-2"
        message={"Congrats! Review is posted."}
        show={showToast}
        severity={"success"}
        setShowToast={setShowToast}
      />
    </>
  );
};

export default withRouter(Review);
