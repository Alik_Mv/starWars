import React, { useState, useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import MoviesList from "../../Components/MoviesList/MoviesList";
import MovieInfo from "../../Components/MovieInfo/MovieInfo";
import Toast from "../../Components/Toast/Toast";
import { requestHandler, useHttp } from "../../Hooks/http.hook";
import s from "./Main.scss";

const Main = () => {
  const { isLoading, request } = useHttp();
  const [movie, setMovie] = useState(null);
  const [movies, setMovies] = useState([]);
  const [showToast, setShowToast] = useState(false);
  const [toastMessage, setToastMessage] = useState("");
  const [toastSeverity, setToastSeverity] = useState("default");

  useEffect(() => {
    loadMovies();
  }, []);

  const toastTrigger = (message, severity) => {
    setShowToast(true);
    setToastMessage(message);
    setToastSeverity(severity);
  };

  const loadMovies = async () => {
    const response = await requestHandler(request, { method: "getMoviesList" });
    if (response.success) {
      setMovies(response.data);
      toastTrigger("All movies has been loaded", "success");
    } else {
      toastTrigger(response.message, "error");
    }
  };

  return (
    <>
      <div className="container">
        <div className="row border rounded">
          <div className="col-xs-2 col-md-3 col-lg-3 col-xl-2  no-padding">
            <MoviesList
              isLoading={isLoading}
              list={movies}
              setMovie={setMovie}
            />
          </div>
          <div className="col-xs-10 col-md-9 col-lg-9 col-xl-10 border-left">
            <div className="row">
              <MovieInfo movie={movie} toastTrigger={toastTrigger} />
            </div>
            {!isNaN(parseInt(movie)) ? (
              <div className="row">
                <Link
                  className={"btn btn-primary btn-block no-padding m-2"}
                  to={{
                    pathname: "/review",
                    params: movie
                  }}
                >
                  {"Leave review"}
                </Link>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>

      <div className={s.toasts}>
        <Toast
          show={showToast}
          setShowToast={setShowToast}
          message={toastMessage}
          severity={toastSeverity}
        />
      </div>
    </>
  );
};

export default withRouter(Main);
