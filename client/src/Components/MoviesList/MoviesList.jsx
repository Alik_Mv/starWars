import React, { useState } from "react";
import s from "./MoviesList.scss";

const MoviesList = ({ isLoading, list, setMovie }) => {
  const [choosedId, setChoosedId] = useState(null);

  const getLoader = () => (
    <div class="spinner-border m-5" role="status">
      <span class="sr-only">{"Loading..."}</span>
    </div>
  );

  const getListItem = (itemInfo) => {
    const { title, index } = itemInfo;
    const choosed = choosedId === index;
    return (
      <li
        className={`${s.liItem} mb-1`}
        onClick={() => {
          setChoosedId(index);
        }}
      >
        {choosed ? <b>{title}</b> : <u>{title}</u>}
      </li>
    );
  };

  return (
    <div className="container">
      <ul className="list-unstyled">
        {isLoading
          ? getLoader()
          : list &&
            list.map((item, index) => {
              return getListItem({ ...item, index });
            })}
      </ul>
      <div className="w-100"></div>
      <button
        onClick={() => {
          setMovie(choosedId);
          localStorage.setItem(
            'movie',
            list.find((item, index) => (
              index === choosedId
            )).title
          );
        }}
        type="button"
        className="btn btn-primary btn-block no-padding mb-2"
        disabled={isNaN(parseInt(choosedId))}
      >
        {"Select episode"}
      </button>
    </div>
  );
};

export default MoviesList;
