import React from "react";
import { Toast as BToast } from "react-bootstrap";
import s from "./Toast.scss";

const Toast = ({
  show,
  setShowToast,
  message,
  severity = "default",
}) => {
  const getSeverityClass = (severityType) =>
    ({
      error: s.toastError,
      success: s.toastSuccess,
      default: "",
    }[severityType]);

  const capitalizeFirst = (word) => {
    return (
      (word instanceof String || typeof word === "string") &&
      `${word.charAt(0).toUpperCase()}${word.slice(1)}`
    );
  };

  return (
    <BToast
      className={getSeverityClass(severity)}
      onClose={() => setShowToast(false)}
      show={show}
      delay={3000}
      autohide
    >
      <BToast.Header>
        <strong className="mr-auto">
          {capitalizeFirst(severity)}
        </strong>
      </BToast.Header>
      <BToast.Body>{message}</BToast.Body>
    </BToast>
  );
};

export default Toast;
