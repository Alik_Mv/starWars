import React, { useMemo, useState } from "react";
import starWarsLogo from "../../../media/starWars.jpg";
import { useHttp, requestHandler } from "../../Hooks/http.hook";
import s from "./MovieInfo.scss";

const MovieInfo = ({ movie: id, toastTrigger }) => {
  const [movie, setMovie] = useState(null);
  const { isLoading, request } = useHttp();

  const getMovie = async () => {
    const response = await requestHandler(request, { method: "getMovie", id });
    const { success } = response;
    if (success) {
      setMovie(response.data);
    } else {
      toastTrigger(response.message, "error");
    }
  };

  useMemo(() => {
    !isNaN(parseInt(id)) && getMovie();
  }, [id]);

  const getLoader = () => (
    <div class="spinner-border m-3" role="status">
      <span class="sr-only">{"Loading..."}</span>
    </div>
  );

  const getPreview = () => {
    return (
      <div className="row m-3 h5">
        <span>{"Please, choose an episode for get more details about it"}</span>
      </div>
    );
  };

  const getInfoBlock = () => {
    const { title, opening_crawl: description } = movie || {};
    return isLoading ? (
      getLoader()
    ) : (
      <div className="row m-3">
        <div className="row">
          <span className="h1">{title}</span>
        </div>
        <div className="row">
          <span>{description}</span>
        </div>
      </div>
    );
  };

  return (
    <div className="container-pd-0 justify-content-start">
      <img src={starWarsLogo} className="img-fluid" alt="Responsive image" />
      <div className="container-fluid">
        {isNaN(parseInt(id)) ? getPreview() : getInfoBlock()}
      </div>
    </div>
  );
};

export default MovieInfo;
