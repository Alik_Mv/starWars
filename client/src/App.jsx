import React from "react";
import { Switch, Route } from "react-router-dom";
import loadable from '@loadable/component'
import { ProgressBar } from "react-bootstrap";

const App = () => {
  const Loading = () => (
    <div className="container m-2">
      <ProgressBar animated now={100} />
    </div>
  );
  const Main =  loadable(() => import('./Pages/Main/Main'), { fallback: <Loading /> });
  const Review =  loadable(() => import('./Pages/Review/Review'), { fallback: <Loading /> });

  return (
    <Switch>
      <Route exact path="/" component = { Main } />
      <Route path="/review" component = { Review } />
    </Switch>
  );
};

export default App;
