import config from 'config';
import { Router } from "express";
import { makeGetRequest as makeRequest } from "../../Utils/makeRequest";

const router = Router();
const { apiPath } = config;

const getMoviesList = async () => {
  const response = await makeRequest(apiPath);
  const { success } = response;
  if (success) {
    const {
      data: { body },
    } = response;
    return {
      data: body.results,
      success,
    };
  } else {
    return response;
  }
};

const getMovieById = async (id) => {
  const response = await makeRequest(`${apiPath}${id + 1}/`);
  const { success } = response;
  if (success) {
    const {
      data: { text },
    } = response;
    const data = JSON.parse(text);
    return {
      data,
      success,
    };
  } else {
    return response;
  }
};

const bodyHandler = async (body, res) => {
  if (!Object.keys(body).length) {
    return makeError("Unknown format of request's body");
  }
  const { method } = body;

  switch (method) {
    case "getMoviesList": {
      const result = await getMoviesList();
      res.status(result.success ? 200 : 500).json(result);
      break;
    }
    case "getMovie": {
      const { id } = body;
      const result = await getMovieById(id);
      res.status(result.success ? 200 : 500).json(result);
      break;
    }
  }
};

router.post("/movies", async (req, res) => {
  try {
    bodyHandler(req.body, res);
  } catch (e) {
    res.status(500).json(makeError(e.message));
  }
});

export { router as moviesService };
