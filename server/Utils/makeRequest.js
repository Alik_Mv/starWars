import superagent from "superagent";

const makeGetRequest = (path) => {
  return superagent
    .get(path)
    .set("Content-Type", "application/json")
    .set("Vary", "Accept")
    .then((data) => {
      return {
        data,
        success: true,
      };
    })
    .catch((e) => {
      return makeError(e.message);
    });
};

const makeError = (message) => {
  return {
    message,
    success: false,
  };
};

export { makeGetRequest };
