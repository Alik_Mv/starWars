import 'ignore'
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import express from 'express';
import config from 'config';
import path from "path";
import fs from 'fs';
import App from '../client/src/App';
import { moviesService } from './Routes/Services/moviesService';
import { ChunkExtractor } from '@loadable/server'

const app = express();
const __dirname = path.resolve();
const { port } = config || 5000;

app.use(express.static(path.resolve(__dirname, 'build/client')));

app.use(express.json({ extended: true }))

app.get("*", (req, res) => {
  const statsFile = path.resolve(__dirname, 'build/server/loadable-stats.json');
  const extractor = new ChunkExtractor({ statsFile })
  const context = {};
  const app = ReactDOMServer.renderToString(
    extractor.collectChunks(
      <StaticRouter>
        <App />
      </StaticRouter>
    )
  );
  const indexFile = path.resolve(__dirname, 'build/client/index.html');
  fs.readFile(indexFile, "utf8", (err, data) => {
    res.set('Content-Type', 'text/html');
    if (err) {
      return res
        .status(500)
        .send("Something error happened. Try go to home page or visit us later!");
    }

    return res.send(
      data.replace('<div id="root"></div>', `<div id="root">${app}</div>`)
    );
  });
});

app.use("/api", moviesService);

app.listen(port, () => console.log(`App started at port ${port}`));
