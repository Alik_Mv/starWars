const webpack = require("webpack");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const WebpackShellPluginNext = require("webpack-shell-plugin-next");
const HtmlWebpackTagsPlugin = require("html-webpack-tags-plugin");
const getCSSModuleLocalIdent = require("react-dev-utils/getCSSModuleLocalIdent");
const LoadablePlugin = require('@loadable/webpack-plugin')

const clientPlugins = (isDev, templatePath, faviconPath, buildPath) => {
  const allPlugins = [
    new HTMLWebpackPlugin({
      template: templatePath,
      collapseWhitespace: !isDev,
      cache: false,
      minify: !isDev,
    }),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: faviconPath,
          to: buildPath,
        },
        {
          from: "node_modules/bootstrap/dist/css",
          to: buildPath + "/css",
        },
      ],
    }),
    new MiniCssExtractPlugin({
      filename: filename(isDev, "css"),
      chunkFilename: "[id].css",
    }),
    new HtmlWebpackTagsPlugin({
      tags: ["css/bootstrap.min.css"],
    }),
  ];

  return allPlugins;
};

const serverPlugins = (isDev) => {
  const allPlugins = [
    new CleanWebpackPlugin(),
    new WebpackShellPluginNext({
      onBuildExit: {
        scripts: [
          "nodemon ./build/server/server.js --watch build/server",
          "open-cli http://localhost:5000",
        ],
        blocking: false,
        parallel: true,
      },
    }),
    new MiniCssExtractPlugin({
      filename: filename(isDev, "css"),
      chunkFilename: "[id].css",
    }),
    new LoadablePlugin(),
  ];

  return allPlugins;
};

const clientOptimization = (isDev) => ({
  minimize: !isDev,
  minimizer: [
    new TerserPlugin(),
    new CssMinimizerPlugin({
      test: /\.css$/i,
    }),
  ],
});

const serverOptimization = (isDev) => ({
  minimize: !isDev,
  minimizer: [new TerserPlugin()],
});

const filename = (isDev, ext) => {
  return `[name].${isDev ? ext : `[hash].${ext}`}`;
};

const defaultCssLoaders = (isDev, isMiniCssEnable) => {
  const loaders = [];
  isMiniCssEnable &&
    loaders.push({
      loader: MiniCssExtractPlugin.loader,
    });

  loaders.push(
    {
      loader: "css-loader",
      options: {
        sourceMap: !isDev,
        modules: {
          localIdentName: "[local]___[hash:base64:5]",
          getLocalIdent: getCSSModuleLocalIdent,
        },
      },
    },
    {
      loader: "postcss-loader",
      options: {
        sourceMap: !isDev,
        postcssOptions: {
          plugins: [["postcss-preset-env"]],
        },
      },
    }
  );

  return loaders;
};

const sassLoader = (isDev) => {
  return {
    loader: "sass-loader",
    options: {
      implementation: require("sass"),
      sourceMap: !isDev,
      sassOptions: {
        fiber: require("fibers"),
      },
    },
  };
};

const mode = (isDev) => ({
  mode: isDev ? "development" : "production",
});

module.exports = {
  clientPlugins,
  serverPlugins,
  clientOptimization,
  serverOptimization,
  filename,
  defaultCssLoaders,
  sassLoader,
  mode,
};
